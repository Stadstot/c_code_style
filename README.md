## Общие правила

### Пробелы
Унарные операторы с операндом пробелом не разделяются:  
`!p    ~b    ++i    j--    (void *)ptr    *p    &x    -k`  
  
Бинарные и тернарные операторы разделяются с операндами 1 пробелом слева и справа:  
`c1 == c2    x + y    i += 2    n > 0 ? n : -n`  
За исключением следующих случаев:
* `s.foo` Доступ к полям структуры
* `p->foo` Доступ к полям структуры по указателю
  
Скобки не обрамляются дополнительными пробелами
* `a[i]` Доступ к элементу массива
* `foo(x, y, z)` Вызов функции

Допускается расстановка дополнительных пробелов и переносов строк для увеличения 
читаемости кода

### Отступы
Отступы должны быть кратны 4 пробелам.
Для формирования отступа допустимо использование только символа пробела.
Пример:
```c
void foo(uint8_t *ptr, uint8_t count) {
    for (uint8_t i = 0; i < count; i++) {
        if ((ptr[i] % 2) == 0) {
            bar(ptr[i]);
        }
    }
}
```

### Имена
При формировании имен файлов, типов, струтур, функций, переменных должны 
использоваться 'CamelCase' и 'lowerCamelCase' нотации. [Wiki](https://ru.wikipedia.org/wiki/CamelCase)  
Пользовательские типы, объявления структур, объявление объединений - 'CamelCase'.  
Имена файлов, переменных, функций - 'lowerCamelCase'.  
При формировании констант, перечислений и макроопределений допускается использование
следующих символов [A..Z], [0..9] и [ \_ ]  
При создании глобальных переменных и функций использование "общих" слов (init, 
start, counter) без уточнения не допустимо. (Плохо: `Image create();` Хорошо: 
`Image imageCreate()`)  
Допускается исользование [ \_ ] в именах глобальных функций и переменных для разделения 
имени функции или переменной и модуля к которым они относятся, при этом желательно 
имя модуля указывать первым.  
Также стоит ознакомится со списком зарезервированых имен: 
[Reserved names](https://www.gnu.org/software/libc/manual/html_node/Reserved-Names.html)

### Типы данных
Желательно использовать целочисленные типы данных определенного размера 
(вместо int), данные типы определены в стандартной библотеке stdint  

| exact size | signed  | unsigned |
| ---------- | ------- | -------- |
| 8 bit      | int8_t  | uint8_t  |
| 16 bit     | int16_t | uint16_t |
| 32 bit     | int32_t | uint32_t |
| 64 bit     | int64_t | uint64_t |

## Примеры использования различных конструкций

### Оператор if
```c
if (x < 0) {
    z = 25;
}

if (--cnt == 0) {
    z = 10;
    cnt = 1000;
} else {
    z = 200;
}

if (x > y) {
    foo(x, y);
    z = 100;
} else if (y < x) {
    foo(y, x);
    z = 200;
}
```
  
### Оператор for
```c
for (i = 0; i < MAX_ITER; ++i) {
    *p2++ = *p1++;
    xx[i] = 0;
}
```
  
### Оператор while
```c
while (--ctr != 0) {
    *p2++ = *p1++;
    *p3++ = 0;
}
```
  
### Оператор do..while
```c
do {
    --ctr;
    *p2++ = *p1++;
} while (cnt > 0);
```

### Оператор switch
```c
switch (key) {
    case KEY_BS: {
        if (--me->ctr == 0) {
            me->ctr = PERIOD;
        }
        break;
    }
    case KEY_CR: {
        ++me->crCtr;
    }
    case KEY_LF:
        ++p;
        break;
    default: {
        Q_ERROR();
        break;
    }
}
```

### Функция
```c
void clrBuf(char *buf[], int len) {
    char *b = &buf[0];
    while (len-- != 0) {
        *b++ = '\0';
    }
}

```

## Комментирование
Текст комментариев должен быть на английском языке.
Описание функций, структур и перечислений производится в заголовочном файле.  
Комментарии в файлах исходных кодов допустимы, но не желательны.  
Комментарии должны быть выполнены в формате системы документирования 
[Doxygen](http://www.stack.nl/~dimitri/doxygen/index.html)  
Пример комментирования стандартных блоков кода (приведен далеко не весь 
функционал Doxygen. Полный список доступен 
[на сайте проекта](http://www.stack.nl/~dimitri/doxygen/manual/index.html)):
```c
/**
 * @file doxygen_c.h
 * @author My Self
 * @date 9 Sep 2012
 * @brief File containing example of doxygen usage for quick reference.
 *
 * Here typically goes a more extensive explanation of what the header
 * defines. Doxygens tags are words preceeded by either a backslash @\
 * or by an at symbol @@.
 * @see http://www.stack.nl/~dimitri/doxygen/docblocks.html
 * @see http://www.stack.nl/~dimitri/doxygen/commands.html
 */
 
/**
 * @brief Use brief, otherwise the index won't have a brief explanation.
 *
 * Detailed explanation.
 */
typedef enum BoxEnum_enum {
    BOXENUM_FIRST,  /**< Some documentation for first. */
    BOXENUM_SECOND, /**< Some documentation for second. */
    BOXENUM_ETC     /**< Etc. */
} BoxEnum;

/**
 * @brief Use brief, otherwise the index won't have a brief explanation.
 *
 * Detailed explanation.
 */
typedef struct BoxStruct_struct {
    int a;    /**< Some documentation for the member BoxStruct#a. */
    int b;    /**< Some documentation for the member BoxStruct#b. */
    double c; /**< Etc. */
} BoxStruct;

/**
 * @brief Example showing how to document a function with Doxygen.
 *
 * Description of what the function does. This part may refer to the parameters
 * of the function, like @p param1 or @p param2.
 * Sometimes it is also convenient to include an example of usage:
 * @code
 * BoxStruct *out = Box_The_Function_Name(param1, param2);
 * printf("something...\n");
 * @endcode
 * @param param1 Description of the first parameter of the function.
 * @param param2 The second one, which follows @p param1.
 * @return Describe what the function returns.
 */
BoxStruct * Box_The_Function_Name(BoxParamType1 param1, BoxParamType2 param2);
```
